// data "http" "install" {
//   url = "https://raw.githubusercontent.com/argoproj/argo-cd/v${var.argo_version}/manifests/install.yaml"
// }

// locals {
//   resources = split("\n---\n", data.http.install.body)
// }

resource "digitalocean_firewall" "firewall" {
  name = "${var.name}-firewall"

  inbound_rule {
    protocol = "tcp"
    port_range = "80"
    source_addresses = [ var.allowlist ]
  }

  tags = [digitalocean_tag.tag.id]
}

resource "digitalocean_kubernetes_cluster" "k8s" {
  name = "${var.name}-k8s"
  region = var.region
  version = var.kubernetes_version
  vpc_uuid = digitalocean_vpc.vpc.id

  node_pool {
    name = "${var.name}-pool"
    size = var.sku
    node_count = 3
    tags = [digitalocean_tag.tag.id]
  }
}

resource "digitalocean_tag" "tag" {
  name = "${var.name}-tag"
}

resource "digitalocean_vpc" "vpc" {
  name = "${var.name}-vpc"
  region = "ams3"
  ip_range = "10.0.0.0/24"
}

module "argo_cd" {
  source = "runoncloud/argocd/kubernetes"

  namespace       = "argocd"
  argo_cd_version = "1.8.7"
}

// resource "k8s_manifest" "argo" {
//   count = length(local.resources)
//   content = local.resources[count.index]
// }

// resource "kubernetes_namespace" "argo" {
//   metadata {
//     name = "argo"
//   }
// }
