variable allowlist {
}
variable argo_version {
    default = "1.8.7"
}
variable kubernetes_version {
    default = "1.20.2-do.0"
}
variable name {
    default = "bonifacius"
}
variable region {
    default = "ams3"
}
variable sku {
    default = "s-1vcpu-2gb"
}
