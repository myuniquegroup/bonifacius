provider "argocd" {
}

provider "digitalocean" {
}

provider "k8s" {
  load_config_file = "false"

  host = digitalocean_kubernetes_cluster.k8s.endpoint

  cluster_ca_certificate = base64decode(
    digitalocean_kubernetes_cluster.k8s.kube_config[0].cluster_ca_certificate
  )
}

provider "helm" {

  kubernetes {
    host = digitalocean_kubernetes_cluster.k8s.endpoint

    cluster_ca_certificate = base64decode(
      digitalocean_kubernetes_cluster.k8s.kube_config[0].cluster_ca_certificate
    )
  }
}

provider "kubernetes" {
  host = digitalocean_kubernetes_cluster.k8s.endpoint
  token = digitalocean_kubernetes_cluster.k8s.kube_config[0].token
  cluster_ca_certificate = base64decode(
    digitalocean_kubernetes_cluster.k8s.kube_config[0].cluster_ca_certificate
  )
}

terraform {
  backend "remote" {
    organization = "myuniqueorganizationname"

    workspaces {
      name = "bonifacius"
    }
  }

  required_providers {
    argocd = {
        source = "oboukili/argocd"
        version = "1.2.0"
    }
    digitalocean = {
      version = "~> 2.5.1"
      source = "digitalocean/digitalocean"
    }
    helm = {
      source = "hashicorp/helm"
      version = "2.0.2"
    }
    k8s = {
      version = ">= 0.8.0"
      source = "banzaicloud/k8s"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.0.2"
    }
  }

  required_version = ">= 0.13.0"
}
